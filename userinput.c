#include "userinput.h"

/*int main(void)
{
	char string[30];
	int integer;
	char character;

	get_str("Input string: ", string, 30);
	get_int("Input integer: ", &integer);
	get_char("Input character: ", &character);

	printf("String: %s\n", string);
	printf("Integer: %i\n", integer);
	printf("Character: %c\n", character);
	
	return 0;
}*/


/**
 * User input functions
 * Author: Ben Longmire - ben@blongmire.com
 */

void get_str(char *prompt, char *dest, int length)
{
	/**
	 * Reads a string from the user
	 * Converts last character to null
	 * Loops if input was longer than expected
	 */
	char *c;

	while(1)
	{
		printf("%s", prompt);
		fgets(dest, length, stdin);
		
		// Terminate string if '\n' found, then return
		if((c = strchr(dest, '\n')))
		{
			c[0] = '\0';
			return;
		}

		// If no '\n' was found, the input was too long
		printf("Your input was too long, please try again\n");
		
		// Clear stdin
		while (fgets(dest, length, stdin) && !strchr(dest, '\n'));
	}
}

void get_int(char *prompt, int *dest)
{
	/**
	 * Reads an integer from user
	 * Loops if input was invalid
	 */
	char input[13] = {0};

	while(1)
	{
		get_str(prompt, input, 13);
		
		if(convert_str_int(dest, input))
		{
			return;
		}
		else
		{
			printf("Please enter a valid integer\n");
		}
	}
}

void get_char(char *prompt, char *dest)
{
	/**
	 * Reads a char from the user
	 * Loops if input was longer than 1 char
	 */
	
	char c[3]; // char + '\n' + '\0'
	while(1)
	{
		printf("%s", prompt);
		fgets(c, 3, stdin);
		char *newline;
		if((newline = strchr(c, '\n')))
		{
			*dest = c[0];
			return;
		}

		// If no '\n' was found, the input was longer than 1 char + '\n'
		printf("Please only input 1 character\n");
		// Clear stdin
		while (fgets(c, 3, stdin) && !strchr(c, '\n'));
	}
	
}

bool is_str_int(char *input)
{
	/**
	 * Checks whether a string is a valid integer
	 * Returns true or false
	 * 
	 * c points to the first character not converted
	 * If it's whitespace or 0, then the input string was a valid number
	 * 
	 * So long as:
	 * 		- tmp is within the limits of integer range
	 *   	- input[0] is a digit or '-', therefore the input wasn't only whitespace
	 */

	char *c;
	int val;
	long tmp = strtol(input, &c, 10);
	
	if ((isspace(*c) || *c == 0)
		&& (tmp >= (long) INT_MIN && tmp <= (long) INT_MAX)
		&& (isdigit(input[0]) || input[0] == '-'))
	{
		return true;
	}

	return false;
}

bool convert_str_int(int *dest, char *input)
{
	/**
	 * Converts string to integer
	 * Returns true on success
	 */

	if(is_str_int(input))
	{
		char *c;
		*dest = (int) strtol(input, &c, 10);
		return true;
	}

	return false;
}